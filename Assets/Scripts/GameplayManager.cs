﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    public bool paused = false;

    public GameObject pauseMenu;
    
    void Start()
    {
        Cursor.visible = false;
    }
    
    void Update()
    {
        //change the pause state with the button "Cancel"
        if (Input.GetButtonDown("Cancel"))
        {
            paused = !paused;
        }
        
        if (paused)
        {
            Cursor.visible = true;
            pauseMenu.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            Cursor.visible = false;
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene("Scenes/Map 1");
    }
}
