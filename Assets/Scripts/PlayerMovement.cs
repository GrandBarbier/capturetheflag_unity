﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    public float acceleration;
    public float airControl;

    public bool isStrafing;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    
    public Vector3 velocity;
    public bool isGrounded;

    public bool isPlayer1;
    
    private float x;
    private float z;


    private void Start()
    {
        // define if it's the player 1 or the player 2
        if (gameObject.name == "Player 1")
        {
            isPlayer1 = true;
        }
        else
        {
            isPlayer1 = false;
        }
    }

    void Update()
    {
        //set isGrounded with the groundCheck
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        
        
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        //set movement values with inputs
        if (isPlayer1)
        {
            x = Input.GetAxis("Horizontal1");
            z = Input.GetAxis("Vertical1"); 
            isStrafing = Input.GetAxis("Strafe1").Equals(1);
            
            if (Input.GetButton("Jump1") && isGrounded)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            }
        }
        else
        {
            x = Input.GetAxis("Horizontal2");
            z = Input.GetAxis("Vertical2");
            isStrafing = Input.GetAxis("Strafe2").Equals(1);
            
            if (Input.GetButton("Jump2") && isGrounded)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity * Time.timeScale);
            }
        }
        
        //set movements velocity on the ground
        if (isGrounded)
        {
            
            if (x < -0.1 || x > 0.1)
            { 
                velocity.x = Mathf.Lerp(velocity.x, x, Time.deltaTime * acceleration);
            }
            else
            { 
                velocity.x = Mathf.Lerp(velocity.x, 0, Time.deltaTime * acceleration); 
            }
            
            if (z < -0.1 || z > 0.1)
            {
                velocity.z = Mathf.Lerp(velocity.z, z, Time.deltaTime * acceleration);
            }
            else
            {
                velocity.z = Mathf.Lerp(velocity.z, 0, Time.deltaTime * acceleration);
            }
        }
        //set movement velocity (air control)
        else
        {
            if (x < -0.1 || x > 0.1)
            {
                velocity.x = Mathf.Lerp(velocity.x, x, Time.deltaTime * airControl);
            }
            
            if (z < -0.1 || z > 0.1)
            {
                velocity.z = Mathf.Lerp(velocity.z, z, Time.deltaTime * airControl);
            }
        }

        Vector3 move;
        //set movement with velocity value when is strafing
        if (isStrafing)
        {
            move = transform.right * velocity.x + transform.forward * velocity.z; 
        }
        //set movement with velocity value when is not strafing
        else
        {
            move = transform.forward * velocity.z;
            
            if (isGrounded)
            {
                transform.Rotate(Vector3.up, x * Time.deltaTime * acceleration / 1.5f);
            }
            else
            {
                transform.Rotate(Vector3.up, x * Time.deltaTime * acceleration / 3f);
            }
        }
        
        //move the character controller
        controller.Move(move * speed * Time.deltaTime);
        
        //set gravity
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }
}
