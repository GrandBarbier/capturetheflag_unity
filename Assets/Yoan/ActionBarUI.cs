﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionBarUI : MonoBehaviour
{
    public float posOnScreenWidth;
    public float posOnScreenHeight;
    
    void Start()
    {
        //DisplayActionBar();
        GetComponent<RectTransform>().position = new Vector3(Screen.width * posOnScreenWidth, Screen.height * posOnScreenHeight, 0);
    }
    
    void Update()
    {
        
    }

    public void DisplayActionBar(PlayerInventory player)
    {
        //Debug.Log("Display Action Bar");
        float offSet = 60f;

        for (int i = 0; i < player.actionSlots.Length; i++)
        {
            GameObject actionSlot = Instantiate(Gears.gears.actionSlotPrefab, transform);

            player.actionSlots[i].slotDisplay = actionSlot;
            
            //Check if i is odd or even
            if (i % 2 == 0)
            {
                actionSlot.GetComponent<RectTransform>().anchoredPosition = new Vector3(offSet, 0, 0);
            }
            else
            {
                actionSlot.GetComponent<RectTransform>().anchoredPosition = new Vector3(-offSet,0, 0);
                offSet += 60f;
            }
        }
    }
}
