﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagSpawner : ItemsWorld
{
    public string team;

    public FlagZone FlagZone;
    
    public override void Start()
    {
        base.Start();
        
       SetItem(null);
    }
    
    void Update()
    {
        
    }

    public override void PickUpItem(PlayerInventory playerInventory, ActionSlot actionSlot)
    {
        if (playerInventory.team != team)
        {
            Debug.Log("Flag world Pick up : " + item);
            item.OnPickUp(playerInventory, actionSlot, this);
        }
    }

    public override void SetItem(Item item)
    {
        Item_Flag flag = (Item_Flag) Gears.gears.ItemFlag.ShallowCopy();

        flag.team = team;
        
        //Set item Funcfor Flag
        this.item = flag;
        ((Item_Flag) this.item).flagZone = FlagZone;
        this.item.carrier = gameObject;
        mesh.transform.localScale = this.item.size;
        text.text = this.item.nameItem + " : Pick up by pressing one of your action";
    }
}
