﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FlagZone : MonoBehaviour
{
    public string team;

    public ParticleSystem ParticleSystem;

    public GameObject flagSlot;

    public IEnumerator timerWin;
    
    void Start()
    {
        timerWin = TimerWin();
        
        ParticleSystem.MainModule m = ParticleSystem.main;
        
        if (team == "TEAM1") { 
            m.startColor = Color.blue;
        }
        else { 
            m.startColor = Color.red;
        }
    }
    
    void Update()
    {
        
    }

    public IEnumerator TimerWin()
    {
        yield return new WaitForSeconds(1f);
        
        Debug.Log(team + " Win");
        timerWin = TimerWin();
    }
    
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerInventory player = col.GetComponent<PlayerInventory>();

            Item_Flag flag = (Item_Flag) Array.Find(player.actionSlots, slot =>
                slot.item?.GetType() == typeof(Item_Flag) || 
                slot.item.GetType().IsSubclassOf(typeof(Item_Flag)))?.item;
            
            //if the player as the same team as the flagZone and have a flag
            if (flag != null && player.team == team)
            {
                player.characterBack.GetComponent<MeshFilter>().mesh = null;
                flagSlot.GetComponent<MeshFilter>().mesh = flag.itemMesh;
                flagSlot.GetComponent<MeshRenderer>().material = flag.itemMat;
                
                //if flag is ennemy flag
                if (flag.team != team)
                {
                    
                    StartCoroutine(timerWin);
                    //Win
                }
                else
                {
                    Instantiate(Gears.gears.flagSpawnerPrefab, transform.position, Gears.gears.flagSpawnerPrefab.transform.rotation, transform);
                }
            }

            /*Debug.Log(player.team == team);
            Debug.Log(Array.Find(player.actionSlots, slot => 
                           (slot.item.GetType() == typeof(Item_Flag) || slot.item.GetType().IsSubclassOf(typeof(Item_Flag)) )) != null);*/
        }
    }
}
