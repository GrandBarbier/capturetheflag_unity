﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gears : MonoBehaviour
{
    public static Gears gears;

    [Header("References")]
    
    public List<PlayerInventory> PlayerInventorys;

    public ActionBarUI ActionBarUi;
    
    //public string[] teams;
    
    public GameObject itemWorldContainer;
    public GameObject miscContainer;
    public GameObject itemWorldTextContainer;
    
    [Header("Prefabs")]

    public GameObject itemWorldPrefab;

    public GameObject flagSpawnerPrefab;

    public GameObject actionSlotPrefab;

    public GameObject minePrefab;

    public GameObject bulletPrefab;

    public GameObject grabPrefab;

    [Header("Items")]

    public Item_Flag ItemFlag;

    public Item_Mine ItemMine;

    public Item_Gun ItemGun;

    public Item_Knive ItemKnive;

    public Item_Chest ItemChest;

    public Item_Food ItemFood;

    public Item_Grab ItemGrab;

    public Item_Boots ItemBoots;

    //public Item[] items = Item_Data.Items;

    void Awake()
    {
        if (gears)
        {
            Destroy(gameObject);
        }
        else
        {
            gears = this;
        }
    }
    
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
}
