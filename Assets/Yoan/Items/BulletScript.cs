﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float speed;
    public int damage;
    
    void Start()
    {
        Destroy(gameObject, 15f);
    }
    
    void Update()
    {
        transform.Translate(transform.forward * speed, Space.World);
    }
    
    private void OnTriggerEnter(Collider col)
    {
        if (!col.isTrigger && col.gameObject.tag == "Player")
        {
            Debug.Log("Bullet damage : " + damage);
            col.GetComponent<PlayerInventory>().TakeDamage(new Damage(damage));
            Destroy(gameObject);
        }
    }
}
