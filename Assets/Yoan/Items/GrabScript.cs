﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabScript : MonoBehaviour
{
    public ActionSlot actionSlotFrom;
    
    public float speed;
    
    void Start()
    {
        Destroy(gameObject, 15f);
    }
    
    void Update()
    {
        transform.Translate(transform.forward * speed, Space.World);
    }
    
    private void OnTriggerEnter(Collider col)
    {
        if (!col.isTrigger && col.gameObject.tag == "Player")
        {
            PlayerInventory playerInventory = col.GetComponent<PlayerInventory>();
            
            int i = Random.Range(0, playerInventory.actionSlots.Length);
            
            Debug.Log("Grab : " + playerInventory.actionSlots[i].item);

            playerInventory.actionSlots[i].item.PickUp(actionSlotFrom.player.GetComponent<PlayerInventory>(), actionSlotFrom);

            playerInventory.actionSlots[i].item = null;

            Destroy(gameObject);
        }
    }
}
