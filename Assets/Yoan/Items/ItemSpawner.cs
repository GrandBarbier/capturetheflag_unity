﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ItemSpawner : MonoBehaviour
{
    public float maxTimer = 10;
    public float timer = 10;

    public Transform itemSpawnPos;

    public ItemsWorld currentItem;

    private static bool t;

    public static List<Item> items = new List<Item>();
    
    void Start()
    {
        FillList();
        //timer = maxTimer;
    }
    
    void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            SpawnNewObject(items);
            timer = maxTimer;
        }
        
        
    }

    public void SpawnNewObject(List<Item> i)
    {
        int r = Random.Range(0, i.Count);
        
        Item itemToSpawn = i[r];

        while (itemToSpawn.carrier != null)
        {
            r = Random.Range(0, i.Count);

            itemToSpawn = i[r];
        }
        
        if (currentItem != null)
        {
            currentItem.DestroyItemWorld();
        }
        
        currentItem = null;
        currentItem = ItemsWorld.CreateItemWorld(itemToSpawn, itemSpawnPos.position).GetComponent<ItemsWorld>();
        //i.Remove(itemToSpawn);
    }

    public void FillList()
    {
        if (!t)
        {
            items.Add(Gears.gears.ItemMine.ShallowCopy());
            items.Add(Gears.gears.ItemGun.ShallowCopy());
            items.Add(Gears.gears.ItemKnive.ShallowCopy());
            items.Add(Gears.gears.ItemChest.ShallowCopy());
            items.Add(Gears.gears.ItemFood.ShallowCopy());
            items.Add(Gears.gears.ItemGrab.ShallowCopy());
            items.Add(Gears.gears.ItemBoots.ShallowCopy());

            t = true;
        }
    }
}
