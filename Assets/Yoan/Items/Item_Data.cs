﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Item_Data
{
    //public static Item[] Items = new Item[] {new Item_Flag(null),  };
}

[Serializable]
public class Item
{
    public GameObject carrier;
    
    public string nameItem;
    
    public Mesh itemMesh;
    public Material itemMat;

    public Sprite ItemSprite;

    public Vector3 size;

    public virtual void UseItem(ActionSlot slot) {
        //Debug.Log("Base UseItem2");
    }

    public Item ShallowCopy()
    {
        //Get a copy of an Item
        return (Item) MemberwiseClone();
    }

    public virtual void OnPickUp(PlayerInventory playerInventory, ActionSlot actionSlot, ItemsWorld itemWorld)
    {
        //Debug.Log("Base On PickUp");
        
        //if the action slot already contains an item
        /*if (actionSlot.item != null && actionSlot.item.nameItem != "" && actionSlot.item.carrier != null && 
            GetType() != typeof(Item_Flag) && !GetType().IsSubclassOf(typeof(Item_Flag)) )
        {
            Debug.Log("Drop Item : " + actionSlot.item);
            ItemsWorld.CreateItemWorld(actionSlot.item, itemWorld.transform.position);
        }*/
        
        playerInventory.ItemsWorldsCloseToPlayer.Remove(itemWorld.gameObject);
        itemWorld.DestroyItemWorld();
        
        PickUp(playerInventory, actionSlot);
    }

    public void PickUp(PlayerInventory playerInventory, ActionSlot actionSlot)
    {
        carrier = playerInventory.gameObject;
        actionSlot.item = this;
        actionSlot.slotDisplay.GetComponent<Image>().sprite = ItemSprite;
    }

    public virtual void ResetItem()
    {
        
    }
}

[Serializable]
public class Item_Flag : Item
{
    [Header("Flag property")]
    public string team;
    
    public FlagZone flagZone;

    public Item_Flag(string team)
    {
        nameItem = "Flag";
        this.team = team;
    }

    public override void OnPickUp(PlayerInventory playerInventory, ActionSlot actionSlot, ItemsWorld itemWorld)
    {
        base.OnPickUp(playerInventory, actionSlot, itemWorld);

        flagZone.flagSlot.GetComponent<MeshFilter>().mesh = null;

        playerInventory.characterBack.GetComponent<MeshFilter>().mesh = itemMesh;
        playerInventory.characterBack.GetComponent<MeshRenderer>().material = itemMat;
        playerInventory.onPlayerDeath += OnCarrierDeath;
    }
    
    public void OnCarrierDeath(int id)
    {
        PlayerInventory player = carrier.GetComponent<PlayerInventory>();
        
        if (player.id == id)
        {
            player.characterBack.GetComponent<MeshFilter>().mesh = null;

            ItemsWorld go = ItemsWorld.CreateItemWorld(ShallowCopy(), carrier.transform.position).GetComponent<ItemsWorld>();
            
            //Find and remove the flag item
            /*foreach (var actionSlot in player.actionSlots)
            {
                if (actionSlot == Array.Find(player.actionSlots, slot => slot.item.GetType() == typeof(Item_Flag) || slot.item.GetType().IsSubclassOf(typeof(Item_Flag))))
                {
                    actionSlot.item = null;
                }
            }*/
            
            go.mesh.transform.localPosition = new Vector3(0, -0.5f, 0);
            go.mesh.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
            //carrier.GetComponent<PlayerInventory>().onPlayerDeath -= OnCarrierDeath;
        }
    }
}

[Serializable]
public class Item_Mine : Item
{
    [Header("Mine Settings")]
    public int damage;
    
    public Item_Mine(int damage)
    {
        nameItem = "Mine";
        this.damage = damage;
    }

    public override void UseItem(ActionSlot slot)
    {
        MonoBehaviour.Instantiate(Gears.gears.minePrefab, slot.player.transform.position, Gears.gears.minePrefab.transform.rotation, Gears.gears.miscContainer.transform).
            GetComponent<MineScript>().damage = damage;
        
        slot.RemoveItem();
    }
}

[Serializable]
public class Item_Food : Item
{
    [Header("Food Settings")] 
    public int heal;

    public Item_Food(int heal)
    {
        nameItem = "Food";
        this.heal = heal;
    }

    public override void UseItem(ActionSlot slot)
    {
        Debug.Log("Eat food");
        slot.player.hp += heal;
    }

    public override void OnPickUp(PlayerInventory playerInventory, ActionSlot actionSlot, ItemsWorld itemWorld)
    {
        UseItem(actionSlot);
        
        playerInventory.ItemsWorldsCloseToPlayer.Remove(itemWorld.gameObject);
        itemWorld.DestroyItemWorld();
    }
}

[Serializable]
public class Item_Gun : Item
{
    [Header("Gun Settings")] 
    public int damage;
    public int ammunition;
    
    public float speed;
    public float fireRate;
    private float fireRateCD;

    public Item_Gun(int damage)
    {
        nameItem = "Gun";
        this.damage = damage;
    }

    public override void UseItem(ActionSlot slot)
    {
        if (fireRateCD <= 0)
        {
            Debug.Log("Use Gun");
            BulletScript bullet = MonoBehaviour.Instantiate(Gears.gears.bulletPrefab, 
                slot.player.transform.position + slot.player.transform.forward * 3f, slot.player.transform.rotation, 
                Gears.gears.miscContainer.transform).GetComponent<BulletScript>();

            bullet.damage = damage;
            bullet.speed = speed;

            fireRateCD = fireRate;
            slot.player.StartCoroutine(CooldownGun());

            ammunition--;
            if (ammunition <= 0)
            {
                slot.RemoveItem();
            }
        }
    }

    public IEnumerator CooldownGun()
    {
        while (fireRateCD > 0)
        {
            //Debug.Log("ReduceCD");
            fireRateCD -= Time.deltaTime;
            yield return null;
        }
    }

    public override void ResetItem()
    {
        ammunition = 6;
    }
}

[Serializable]
public class Item_Knive : Item
{
    [Header("Knive Settings")] 
    public int damage;
    public int nbrOfUse;
    
    public float Aoe;
    public float AttackRate;
    private float AttackRateCd;

    public Item_Knive(int damage)
    {
        nameItem = "Knive";
        this.damage = damage;
    }

    public override void UseItem(ActionSlot slot)
    {
        if (AttackRateCd <= 0)
        {
            Debug.Log("Use Knive");
            RaycastHit[] hit = Physics.SphereCastAll(slot.player.transform.forward * 3, Aoe, slot.player.transform.forward);

            foreach (var hi in hit)
            {
                if (hi.collider.GetComponent<PlayerInventory>() && !hi.collider.isTrigger && hi.collider.gameObject != slot.player.gameObject)
                {
                    Debug.Log("Knive Damage : " + damage);
                    hi.collider.gameObject.GetComponent<PlayerInventory>().TakeDamage(new Damage(damage));
                }
            }

            AttackRateCd = AttackRate;
            slot.player.StartCoroutine(CooldownKnive());
            
            nbrOfUse--;
        
            if (nbrOfUse <= 0)
            {
                slot.RemoveItem();
            }
        }
    }
    
    public IEnumerator CooldownKnive()
    {
        while (AttackRateCd > 0)
        {
            //Debug.Log("ReduceCD");
            AttackRateCd -= Time.deltaTime;
            yield return null;
        }
    }

    public override void ResetItem()
    {
        nbrOfUse = 3;
    }
}

[Serializable]
public class Item_Chest : Item
{
    public Item_Chest()
    {
        nameItem = "Chest";
    }
    
    public override void UseItem(ActionSlot slot)
    {
        slot.player.onTakeDamage += Protect;
    }

    public override void OnPickUp(PlayerInventory playerInventory, ActionSlot actionSlot, ItemsWorld itemWorld)
    {
        base.OnPickUp(playerInventory, actionSlot, itemWorld);
        UseItem(actionSlot);
    }

    public void Protect(Damage dmg)
    {
        Debug.Log("Protect");
        dmg.damage = 0;
        
        //Remove Chest
        foreach (var slot in carrier.GetComponent<PlayerInventory>().actionSlots)
        {
            if (slot.item == this)
            {
                slot.player.onTakeDamage -= Protect;
                slot.RemoveItem();
            }
        }
    }
}

[Serializable]
public class Item_Grab : Item
{
    public int nbrOfUse;
    public float speed;
    public float fireRate;
    private float fireRateCd;
    
    public Item_Grab()
    {
        nameItem = "Grab";
    }
    
    public override void UseItem(ActionSlot slot)
    {
        if (fireRateCd <= 0)
        {
            //Debug.Log("Use Grab");
            
            GrabScript grab = MonoBehaviour.Instantiate(Gears.gears.grabPrefab, slot.player.transform.position + slot.player.transform.forward * 3f,
                slot.player.transform.rotation, Gears.gears.miscContainer.transform).GetComponent<GrabScript>();

            grab.speed = speed;
            grab.actionSlotFrom = slot;

            fireRateCd = fireRate;
            slot.player.StartCoroutine(CooldownGrab());

            nbrOfUse--;
            if (nbrOfUse <= 0)
            {
                slot.RemoveItem();
            }
        }
    }
    
    public IEnumerator CooldownGrab()
    {
        while (fireRateCd > 0)
        {
            //Debug.Log("ReduceCD");
            fireRateCd -= Time.deltaTime;
            yield return null;
        }
    }

    public override void ResetItem()
    {
        nbrOfUse = 3;
    }
}

[Serializable]
public class Item_Boots : Item
{
    public float duration;
    public float speedMultiplier;

    public bool used;

    public Item_Boots()
    {
        nameItem = "Boots";
    }

    public override void UseItem(ActionSlot slot)
    {
        if (!used)
        {
            //Debug.Log("Use Boots");
        
            float speedBase = slot.player.GetComponent<PlayerMovement>().speed;
            slot.player.GetComponent<PlayerMovement>().speed *= speedMultiplier;

            slot.player.StartCoroutine(DurationCount(slot, speedBase));

            used = true;
        }
    }
    
    public override void OnPickUp(PlayerInventory playerInventory, ActionSlot actionSlot, ItemsWorld itemWorld)
    {
        base.OnPickUp(playerInventory, actionSlot, itemWorld);
        UseItem(actionSlot);
    }

    public IEnumerator DurationCount(ActionSlot slot, float speed)
    {
        yield return new WaitForSeconds(duration);
        slot.player.GetComponent<PlayerMovement>().speed = speed;
        slot.RemoveItem();
    }

    public override void ResetItem()
    {
        duration = 10f;
    }
}

