﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemsWorld : MonoBehaviour
{
    public Item item;
    
    public TextMeshProUGUI text;

    public Transform uiPos;

    public GameObject mesh;

    public virtual void Start()
    {
        text.transform.SetParent(Gears.gears.itemWorldTextContainer.transform);
    }
    
    void Update()
    {
       
    }

    public void UpdateTextPos(Camera cam)
    {
        if (!text.isActiveAndEnabled)
        {
            text.gameObject.SetActive(true);
        }
        else
        {
            text.gameObject.GetComponent<RectTransform>().position = cam.WorldToScreenPoint(uiPos.position);
        }
    }

    public virtual void SetItem(Item item)
    {
        this.item = item;
        
        item.carrier = gameObject;
        mesh.transform.localScale = item.size;
        mesh.GetComponent<MeshFilter>().mesh = this.item.itemMesh;
        mesh.GetComponent<MeshRenderer>().material = this.item.itemMat;
        text.text = item.nameItem + " : Pick up by pressing one of your action";
    }

    public virtual void PickUpItem(PlayerInventory playerInventory, ActionSlot actionSlot)
    {
        Debug.Log("Pick up : " + item);
        item.OnPickUp(playerInventory, actionSlot, this);
        //Debug.Log("pick up Item");
        /*if (actionSlot.item != null && actionSlot.item.nameItem != "" && actionSlot.item.carrier != null && 
            item.GetType() != typeof(Item_Flag) && !item.GetType().IsSubclassOf(typeof(Item_Flag)) )
        {
            Debug.Log("Drop Item : " + actionSlot.item);
            CreateItemWorld(actionSlot.item, gameObject.transform.position);
        }
        
        playerInventory.ItemsWorldsCloseToPlayer.Remove(gameObject);
        item.carrier = playerInventory.gameObject;
        actionSlot.item = item;
        actionSlot.slotDisplay.GetComponent<Image>().sprite = item.ItemSprite;
        DestroyItemWorld();*/
    }

    public void DestroyItemWorld()
    {
        foreach (var playerInventory in Gears.gears.PlayerInventorys)
        {
            if (playerInventory.ItemsWorldsCloseToPlayer.Contains(gameObject))
            {
                playerInventory.ItemsWorldsCloseToPlayer.Remove(gameObject);
            }
        }
        
        Destroy(text.gameObject);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerInventory player = col.GetComponent<PlayerInventory>();
            
            //text?.gameObject.SetActive(true);
            //Debug.Log(Camera.main.WorldToScreenPoint(uiPos.position));
            
            player.ItemsWorldsCloseToPlayer.Add(gameObject);
            
            //if the player have an empty slot pickUp Item on this slot
            foreach (var slot in player.actionSlots)
            {
                if (slot.item == null || slot.item.nameItem == "" && slot.item.carrier == null || item.GetType() == typeof(Item_Food))
                {
                    PickUpItem(player, slot);
                    break;
                }
            }
        }
    }
    
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            text?.gameObject.SetActive(false);
            col.gameObject.GetComponent<PlayerInventory>().ItemsWorldsCloseToPlayer.Remove(gameObject);
        }
    }
    
    public static GameObject CreateItemWorld(Item item, Vector3 pos)
    {
        ItemsWorld instanceItemWorld = Instantiate(Gears.gears.itemWorldPrefab, pos, 
            Gears.gears.itemWorldPrefab.transform.rotation, Gears.gears.itemWorldContainer.transform).GetComponent<ItemsWorld>();

        instanceItemWorld.SetItem(item);

        return instanceItemWorld.gameObject;
    }
}
