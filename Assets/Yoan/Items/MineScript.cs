﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineScript : MonoBehaviour
{
    public int damage;

    public ParticleSystem ExplosionParticleSystem;

    private bool active;

    void Start()
    {
        
    }
    
    void Update()
    {
        
    }

    public IEnumerator Explosion()
    {
        ExplosionParticleSystem.Play();
        yield return new WaitForSeconds(ExplosionParticleSystem.main.duration);
        Destroy(gameObject);
    }
    
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player" && active)
        {
            Debug.Log("Mine damage : " + damage);
           col.GetComponent<PlayerInventory>().TakeDamage(new Damage(damage));
           StartCoroutine(Explosion());
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            active = true;
        }
    }
}
