﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class PlayerInventory : MonoBehaviour
{
    //[TextArea(10,10)]
    public int id;
    
    public string team;

    public int hp = 10;

    public ActionBarUI ActionBarUi;

    public GameObject characterBack;

    public Camera camera;

    public Transform spawnPos;
    
    public event Action<int> onPlayerDeath;

    public event Action<Damage> onTakeDamage;

    [Header("ActionBar")]
    public ActionSlot[] actionSlots;

    public List<GameObject> ItemsWorldsCloseToPlayer;

    void Start()
    {
        Gears.gears.PlayerInventorys.Add(this);
        
        for (int i = 0; i < actionSlots.Length; i++)
        {
            //actionSlots[i].index = i;
            actionSlots[i].player = this;

            //Test
            /*if (i == 0)
            {
                actionSlots[i].item = Gears.gears.ItemGrab;
            }
            else
            {
                actionSlots[i].item = Gears.gears.ItemBoots;
                actionSlots[i].item.UseItem(actionSlots[i]);
            }*/
        }
        
        ActionBarUi.DisplayActionBar(this);
    }
    
    void Update()
    {
        //foreach slot in the actionBar check if the key is pressed
        
        for (int i = 0; i < actionSlots.Length; i++)
        {
            if (Input.GetKeyDown(actionSlots[i].key))
            {
                //If there are no ItemWorld Close to the player execute action else pick up the closest ItemWorld's item
                if (ItemsWorldsCloseToPlayer.Count <= 0)
                {
                    Debug.Log("Use Item " + actionSlots[i].item);
                    actionSlots[i].item?.UseItem(actionSlots[i]);
                }
                else
                {
                    GameObject closestItemWorld = FindClosestGameObject(ItemsWorldsCloseToPlayer);
                    //ItemsWorldsCloseToPlayer.Remove(closestItemWorld);
                    closestItemWorld?.GetComponent<ItemsWorld>().PickUpItem(this, actionSlots[i]);
                }
            }
        }
        
        FindClosestGameObject(ItemsWorldsCloseToPlayer)?.GetComponent<ItemsWorld>().UpdateTextPos(camera);

        //Test
        /*if (Input.GetButtonDown("Fire2"))
        {
            Debug.Log("Test");
            TriggerDeath();
        }*/
    }

    public GameObject FindClosestGameObject(List<GameObject> gameObjects)
    {
        GameObject closest = null;
        
        float distance = Mathf.Infinity;

        for (int i = 0; i < gameObjects.Count; i++)
        {
            if (gameObjects[i] != null)
            {
                if (Vector3.Distance(gameObject.transform.position, gameObjects[i].transform.position) < distance) 
                {
                    distance = Vector3.Distance(gameObject.transform.position, gameObjects[i].transform.position);
                    closest = gameObjects[i];
                }
                else {
                    gameObjects[i].GetComponent<ItemsWorld>().text.gameObject.SetActive(false);
                }
            }
        }

        return closest;
    }

    public void TriggerDeath()
    {
        if (onPlayerDeath != null)
        {
            onPlayerDeath(id);
            onPlayerDeath = null;
        }

        foreach (var actionSlot in actionSlots)
        {
            actionSlot.item = null;
            actionSlot.slotDisplay.GetComponent<Image>().sprite = Gears.gears.actionSlotPrefab.GetComponent<Image>().sprite;
        }
        
        //teleport player
        transform.position = spawnPos.position;
    }

    public void TakeDamage(Damage damage)
    {
        if (onTakeDamage != null)
        {
            onTakeDamage(damage);
            Debug.Log("Take damage : " + damage.damage);
            //hp -= damage;
        }

        if (hp <= 0)
        {
            TriggerDeath();
        }
    }
}

[Serializable]
public class ActionSlot
{
    //public KeyCode t;
    //t = (KeyCode) Enum.Parse(typeof(KeyCode), "W");
    public PlayerInventory player;
    public KeyCode key = (KeyCode) Enum.Parse(typeof(KeyCode), "A");
    public Item item;
    public GameObject slotDisplay;
    //public int index;

    public void RemoveItem()
    {
        if (item != null)
        {
            item.ResetItem();
            item.carrier = null;
            item = null;
            slotDisplay.GetComponent<Image>().sprite = Gears.gears.actionSlotPrefab.GetComponent<Image>().sprite;
        }
    }
}

public class Damage
{
    public Damage(int damage)
    {
        this.damage = damage;
    }
    public int damage;
}
